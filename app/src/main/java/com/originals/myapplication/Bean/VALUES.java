package com.originals.myapplication.Bean;

public class VALUES {
    //result and request code
    public static final int NEW_TASK_REQUEST_CODE = 0;
    public static final int TASK_INFO_REQUEST_CODE = 1;
    public static final int TASK_INFO_CHANGE_CODE = 2;

    //item id
    //Kanban
    public static final int CREATE_ITEM = 3;
    public static final int KANBAN_ITEM = 4;
    public static final int DELETE_ITEM = 5;
    public static final int NORMAL_ITEM = 6;
    public static final int WELCOME_ITEM = 7;
    public static final int TASK_DELETE = 8;

    //add new task
    public static final int ADD_DESCRIPTION = 20;
    public static final int ADD_DATELINE = 21;
    public static final int ADD_TAGS = 22;
    public static final int DELETE_TASK = 23;
    public static final int TAGS = 24;
    public static final int NEW_TAG = 25;
}
