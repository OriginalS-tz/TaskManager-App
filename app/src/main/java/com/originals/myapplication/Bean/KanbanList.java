package com.originals.myapplication.Bean;

import java.util.ArrayList;
import java.util.List;

public class KanbanList {
    private List<Kanban> kanbans;

    public KanbanList() {
        kanbans = new ArrayList<>();
    }

    public List<Kanban> getKanbans() {
        return kanbans;
    }

    public void addKanban(Kanban kanban) {
        kanbans.add(kanban);
    }
}
