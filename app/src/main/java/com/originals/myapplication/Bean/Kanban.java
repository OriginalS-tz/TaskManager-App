package com.originals.myapplication.Bean;

import java.util.ArrayList;
import java.util.List;

public class Kanban {
    private String name;
    private List<Task> taskList;

    public Kanban() {
        name = new String();
        taskList = new ArrayList<>();
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void addTask(Task task) {
        taskList.add(task);
    }

    public List<Task> getTaskList() {
        return taskList;
    }
}
