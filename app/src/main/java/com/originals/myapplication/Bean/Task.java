package com.originals.myapplication.Bean;

import android.os.Parcel;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

public class Task implements Serializable{
    private String taskName;
    private boolean dateLineFlag;
    private boolean subTaskListFlag;
    private boolean tagsFlag;
    private boolean descriptionFlag;

    private UUID uuid;
    private String dateLine;
    private String description;
    private List<String> subTaskList;
    private List<String> Tags;

    public Task(String taskName) {
        uuid = UUID.randomUUID();
        this.taskName = taskName;
        dateLineFlag = false;
        subTaskListFlag = false;
        tagsFlag = false;
        descriptionFlag = false;

        subTaskList = new ArrayList<>();
        Tags = new ArrayList<>();
    }

    public void setTask(Task task) {
       taskName = task.taskName;
       dateLineFlag = task.dateLineFlag;
       dateLine = task.dateLine;
       subTaskListFlag = task.subTaskListFlag;
       subTaskList = task.subTaskList;
       tagsFlag = task.tagsFlag;
       Tags = task.Tags;
       descriptionFlag = task.descriptionFlag;
       description = task.description;
    }


    public void setTaskName(String taskName) {
        this.taskName = taskName;
    }

    public String getTaskName() {
        return taskName;
    }


    /**UUID**/

    public UUID getUuid() {
        return uuid;
    }

    public void setUuid(UUID uuid) {
        this.uuid = uuid;
    }


    /**Description**/

    public void setDescription(String description) {
        this.description = description;
    }

    public void setDescription(boolean hasDescription) {
        descriptionFlag = hasDescription;
    }

    public boolean hasDescription() {
        return  descriptionFlag;
    }

    public String getDescription() {
        return description;
    }



    /**DateLine**/

    public void setDateLine(boolean hasDateLine) {
        dateLineFlag = hasDateLine;
    }

    public void setDateLine(String dateLine) {
        this.dateLine = dateLine;
    }

    public boolean hasDateLine() {
        return dateLineFlag;
    }

    public String getDateLine() {
        return dateLine;
    }



    /**Tags**/

    public boolean hasTags() {
        return tagsFlag;
    }

    public void setTags(boolean hasTags) {
        tagsFlag = hasTags;
    }

    public void setTags(List<String> tags) {
        Tags = tags;
    }

    public List<String> getTags() {
        return Tags;
    }

    public void addTag(String tag) {
        Tags.add(tag);
    }


    /**SubTaskList**/

    public boolean hasSubTaskList() {
        return subTaskListFlag;
    }

    public void setSubTaskList(boolean hasSubTaskList) {
        subTaskListFlag = hasSubTaskList;
    }

    public void setSubTaskList(List<String> subTaskList) {
        this.subTaskList = subTaskList;
    }

    public void addSubTask(String subTask) {
        subTaskList.add(subTask);
    }

    public List<String> getSubTaskList() {
        return subTaskList;
    }

}
