package com.originals.myapplication.Controller;

import android.content.Context;

import com.originals.myapplication.Bean.Kanban;
import com.originals.myapplication.Bean.Task;
import com.originals.myapplication.Model.KanbanDbManager;
import com.originals.myapplication.Model.KanbanManager;

import java.util.List;

public class KanbanController {

    private KanbanManager kanbanManager;

    public KanbanController(Context context) {
        kanbanManager = KanbanManager.getInstance(context);
    }

    public KanbanController() {
        kanbanManager = KanbanManager.getInstance();
    }

    public List<Kanban> getKanbanList() {
        return kanbanManager.getmKanbanList().getKanbans();
    }

    public List<Task> getTaskList(int index) {
        Kanban kanban = kanbanManager.getmKanbanList().getKanbans().get(index);
        return kanban.getTaskList();

    }

    public boolean addKanban(Kanban kanban) {
        for (Kanban mKanban : kanbanManager.getmKanbanList().getKanbans()) {
            if (kanban.getName().equals(mKanban.getName())) {
               return false;
            }
        }
        kanbanManager.addKanban(kanban.getName());
        kanbanManager.getmKanbanList().addKanban(kanban);
        return true;
    }

    public void addTask(int currentKanban, Task task) {
        kanbanManager.addTaskToKanban(kanbanManager.getmKanbanList().getKanbans().get(currentKanban).getName(), task);
        kanbanManager.getmKanbanList().getKanbans().get(currentKanban).addTask(task);
    }

    public void changeTask(int currentKanban, int changeInfoPosition, Task task) {
       getTaskList(currentKanban).get(changeInfoPosition).setTask(task);
       kanbanManager.changeTask(kanbanManager.getmKanbanList().getKanbans().get(currentKanban).getName(), task);
    }

    public void deleteKanban(String kanbanName) {
        List<Kanban> list = kanbanManager.getmKanbanList().getKanbans();
        for (Kanban mKanban : list) {
            if (mKanban.getName().equals(kanbanName)) {
                list.remove(mKanban);
                kanbanManager.removeKanban(mKanban.getName());
            }
        }
    }

    public String getKanbanName(int currentKanban) {
       return  kanbanManager.getmKanbanList().getKanbans().get(currentKanban).getName();
    }

    public void deleteTask(int currentKanban, int changeInfoPosition) {
        Task task = getTaskList(currentKanban).get(changeInfoPosition);
        getTaskList(currentKanban).remove(changeInfoPosition);
        kanbanManager.removeTask(task.getUuid());
    }

}
