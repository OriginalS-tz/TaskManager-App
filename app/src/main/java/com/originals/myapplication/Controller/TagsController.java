package com.originals.myapplication.Controller;

import android.content.Context;

import com.originals.myapplication.Model.TagsManager;

import java.util.List;

public class TagsController {
    TagsManager tagsManager;

    public TagsController() {
        tagsManager = TagsManager.getInstance();
    }

    public TagsController(Context context) {
        tagsManager = TagsManager.getInstance(context);
    }

    public boolean addTag(String newTag) {
        if (newTag.equals(""))
            return false;
        for (String tag : getTags()) {
            if (newTag.equals(tag))
                return false;
        }
        tagsManager.addTag(newTag);
        return true;
    }

    public void removeTag(String tag) {
        tagsManager.removeTag(tag);
        //tagsManager.getTags().remove(tag);
    }

    public List<String> getTags() {
       return tagsManager.getTags();
    }
}
