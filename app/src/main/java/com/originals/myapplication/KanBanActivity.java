package com.originals.myapplication;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Color;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v7.widget.Toolbar;
import android.view.ContextMenu;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.SubMenu;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Toast;

import com.originals.myapplication.Controller.KanbanController;
import com.originals.myapplication.Bean.Kanban;
import com.originals.myapplication.Bean.Task;
import com.originals.myapplication.Bean.VALUES;

public class KanBanActivity extends Fragment {
    private int currentKanbanType = VALUES.WELCOME_ITEM;
    private int changePosition = 0;
    private int currentKanban = 0;
    private Toolbar toolbar;
    private View v;
    private FloatingActionButton FAB;
    private AlertDialog newKanbanDialog;

    private KanbanController kanbanController;

    private Context mContext;
    private SQLiteDatabase mDB;

    private static final int ITEM1 = 1;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //kanbanController = new KanbanController();
    }

    @Override
    public View onCreateView(LayoutInflater inflater,  ViewGroup container, Bundle savedInstanceState) {
        v = inflater.inflate(R.layout.kanban_list, container,false);

        kanbanController = new KanbanController(v.getContext().getApplicationContext());

        initDB();
        initList();
        initToolBar();
        initFAB();
        initDialog();
        return v;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (data != null) {
            Task task = (Task) data.getSerializableExtra("task");
            if (resultCode == VALUES.NEW_TASK_REQUEST_CODE) {
                addTask(task);
            } else if (resultCode == VALUES.TASK_INFO_CHANGE_CODE) {
                Toast.makeText(v.getContext(), "change ", Toast.LENGTH_LONG).show();
                changeInfo(task);
            }
        } else if (resultCode == VALUES.DELETE_TASK){
            removeTask();
        }
    }

    @Override
    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
        super.onCreateContextMenu(menu, v, menuInfo);
        SubMenu subMenu =  menu.addSubMenu(0,VALUES.NORMAL_ITEM,0,"Kanban List");
        menu.add(0, VALUES.CREATE_ITEM,0,"New Kanban");
        if (currentKanbanType == VALUES.KANBAN_ITEM) {
            menu.add(0,VALUES.DELETE_ITEM,0,"Delete This Kanban");
        }

        int index = 0;
        for (Kanban kanban : kanbanController.getKanbanList()) {
            if (index == 0) {
                subMenu.add(0,VALUES.WELCOME_ITEM,0,kanban.getName());
                index++;
            } else {
                subMenu.add(0,VALUES.KANBAN_ITEM,0, kanban.getName());
            }
        }
    }

    @Override
    public boolean onContextItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case VALUES.CREATE_ITEM: addKanban();break;
            case VALUES.WELCOME_ITEM: case VALUES.KANBAN_ITEM: {
                currentKanbanType = item.getItemId();
                currentKanban = 0;
                for (Kanban kanban : kanbanController.getKanbanList()) {
                    //find the kanban form kanban list
                    if (kanban.getName() == item.getTitle()) {
                        toolbar.setTitle(item.getTitle());
                        //Toast.makeText(this.v.getContext(), item.getTitle(), Toast.LENGTH_SHORT).show();
                        initList();
                        break;
                    } else {
                        currentKanban++;
                    }
                }
                break;
            }
            case VALUES.DELETE_ITEM: {
                Toast.makeText(this.v.getContext(),"delete the kanban",Toast.LENGTH_SHORT).show();
                kanbanController.deleteKanban(kanbanController.getKanbanName(currentKanban));
                currentKanban--;
                if (currentKanban == 0)
                    currentKanbanType = VALUES.WELCOME_ITEM;
                toolbar.setTitle(kanbanController.getKanbanName(currentKanban));
                initList();
            }
        }
        return true;
    }

    private void initDB() {
        mContext = v.getContext().getApplicationContext();
    }

    private void initFAB() {
        FAB = v.findViewById(R.id.fab);
        FAB.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                addTask();
            }
        });
    }

    private void initList() {
        ListView mView=(ListView)v.findViewById(R.id.ListView);
        mView.setClickable(true);

        mView.setOnItemClickListener(
                new AdapterView.OnItemClickListener() {
                    @Override
                    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                        Task task = kanbanController.getTaskList(currentKanban).get(position);
                        Intent intent = new Intent(getActivity(), TaskDetailFragment.class);
                        intent.putExtra("task", task);
                        startActivityForResult(intent, VALUES.TASK_INFO_REQUEST_CODE);
                        initList();
                        changePosition = position;
                        Toast.makeText(v.getContext(), "post="+position, Toast.LENGTH_LONG).show();
                    }
                }
        );

        TaskListAdapter mAdapter=new TaskListAdapter(v.getContext(), kanbanController.getTaskList(currentKanban));
        mView.setAdapter(mAdapter);
    }

    private void initToolBar() {
        toolbar = (Toolbar) v.findViewById(R.id.toolbar);
        toolbar.setTitleTextColor(Color.WHITE);
        toolbar.setTitle(kanbanController.getKanbanName(currentKanban));

        toolbar.inflateMenu(R.menu.kanban_menu);
        this.registerForContextMenu(toolbar);

        Toolbar.OnMenuItemClickListener listener = new Toolbar.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem item) {
                String meg = "hello";
                switch (item.getItemId()) {
                    case R.id.kanban_list:
                        toolbar.showContextMenu();
                        break;
                }
                return true;
            }
        };
        toolbar.setOnMenuItemClickListener(listener);
    }

    private void initDialog() {
        final EditText editText = new EditText(v.getContext());
        AlertDialog.Builder builder = new AlertDialog.Builder(v.getContext());

        builder.setTitle("Kanban Name");
        builder.setView(editText);

        builder.setPositiveButton("create", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                Kanban kanban = new Kanban();
                String kanbanName = editText.getText().toString();
                if (kanbanName.length() > 0) {
                    kanban.setName(kanbanName);
                    if (kanbanController.addKanban(kanban)) {
                        toolbar.setTitle(kanbanName);
                        currentKanban = kanbanController.getKanbanList().size() - 1;
                        currentKanbanType = VALUES.KANBAN_ITEM;
                        initList();
                    } else {
                        Toast.makeText(v.getContext(), "the kanban already exist", Toast.LENGTH_LONG).show();

                    }
                }
                editText.setText("");
            }
        });
        builder.setCancelable(true);

        newKanbanDialog = builder.create();
        newKanbanDialog.setCanceledOnTouchOutside(true);
    }

    private void addKanban() {
        newKanbanDialog.show();
    }

    private void addTask() {
        Intent intent = new Intent(getActivity(), TaskDetailFragment.class);
        startActivityForResult(intent, VALUES.NEW_TASK_REQUEST_CODE);
    }

    private void addTask(Task task) {
        kanbanController.addTask(currentKanban, task);
        initList();
    }

    private void changeInfo(Task task) {
        kanbanController.changeTask(currentKanban, changePosition, task);
        initList();
    }

    private void removeTask() {
        kanbanController.deleteTask(currentKanban, changePosition);
        initList();
    }
}
