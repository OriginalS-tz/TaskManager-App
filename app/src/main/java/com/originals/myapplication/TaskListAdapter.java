package com.originals.myapplication;
import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.originals.myapplication.Bean.Task;
import com.yang.flowlayoutlibrary.FlowLayout;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

public class TaskListAdapter extends BaseAdapter {
    private Context mContext;
    private List<Task> taskList;
    public TaskListAdapter(Context mContext, List<Task> taskList){
        this.mContext = mContext;
        this.taskList = taskList;
    }

    @Override
    public int getCount() {
        return taskList.size();
    }

    @Override
    public Object getItem(int Index) {
        return taskList.get(Index);
    }

    @Override
    public long getItemId(int Index) {
        return Index;
    }

    @Override
    public View getView(final int Index, View mView, ViewGroup mParent) {
        mView = LayoutInflater.from(mContext).inflate(R.layout.list_item, null);
        TextView taskName = (TextView) mView.findViewById(R.id.TaskName);

        Task task = taskList.get(Index);

        taskName.setText(task.getTaskName());
        if (task.hasTags()) {
            setTags(mView, Index);
        }

        TextView leftTime = mView.findViewById(R.id.LeftTime);
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
        try {
            if (task.hasDateLine()) {
                Date  date = format.parse(task.getDateLine());
                Date now = new Date();
                if (date.after(now)) {
                    int month = date.getMonth() - now.getMonth();
                    int day = date.getDay() - now.getDay();
                    if (month == 0) {
                        leftTime.setText(day + " d");
                    } else {
                        leftTime.setText(month + " M " + day + " d");
                    }
                } else {
                    leftTime.setText(task.getDateLine());
                }
            }

        } catch (ParseException e) {
            e.printStackTrace();
        }

        return mView;
    }
    
    private void setTags(View mView, int index) {
        FlowLayout flKeyword = mView.findViewById(R.id.fl_keyword);

        // 关键字集合
        List<String> list = taskList.get(index).getTags();

        // 设置文字大小
        flKeyword.setTextSize(10);
        // 设置文字颜色
        flKeyword.setTextColor(Color.BLACK);
        // 设置文字背景
        flKeyword.setBackgroundResource(R.drawable.bg_frame);

        // 设置文字水平margin
        flKeyword.setHorizontalSpacing(2);
        // 设置文字垂直margin
        flKeyword.setVerticalSpacing(5);

        // 设置文字水平padding
        flKeyword.setTextPaddingH(15);
        // 设置文字垂直padding
        flKeyword.setTextPaddingH(8);

        // 设置UI与点击事件监听
        // 最后调用setFlowLayout方法
        flKeyword.setFlowLayout(list, new FlowLayout.OnItemClickListener() {
            @Override
            public void onItemClick(String content) {}
        });
    }

}

