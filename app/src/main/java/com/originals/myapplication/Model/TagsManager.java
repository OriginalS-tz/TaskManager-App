package com.originals.myapplication.Model;

import android.content.Context;

import java.util.ArrayList;
import java.util.List;

public class TagsManager {

    private List<String> tags;
    private static TagsManager singleTon;
    private KanbanDbManager kanbanDbManager;

    private TagsManager(Context context) {
        kanbanDbManager = KanbanDbManager.getInstance(context);
        tags = kanbanDbManager.getTags();
    }

    private TagsManager() {
        tags = new ArrayList<>();
        //get the tags from db

        //test
        tags.add("Today");
        tags.add("tomorrow");
    }

    public static TagsManager getInstance(Context context) {
        if (singleTon == null)
            singleTon = new TagsManager(context);
        return  singleTon;
    }

    public static TagsManager getInstance() {
        if (singleTon == null)
            singleTon = new TagsManager();
        return singleTon;
    }

    public List<String> getTags() {
        return tags;
    }

    public void removeTag(String tag) {
        singleTon.getTags().remove(tag);
        kanbanDbManager.removeTag(tag);
    }

    public void addTag(String tag) {
        tags.add(tag);
        kanbanDbManager.addNewTag(tag);
    }
}
