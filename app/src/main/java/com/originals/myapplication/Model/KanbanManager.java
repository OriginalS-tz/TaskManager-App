package com.originals.myapplication.Model;

import android.content.Context;

import com.originals.myapplication.Bean.Kanban;
import com.originals.myapplication.Bean.KanbanList;
import com.originals.myapplication.Bean.Task;

import java.util.List;
import java.util.UUID;

public class KanbanManager {
    private static KanbanManager singleTon;
    private KanbanList mKanbanList;
    private KanbanDbManager kanbanDbManager;

    private KanbanManager(Context context) {
        kanbanDbManager = KanbanDbManager.getInstance(context);
        mKanbanList = kanbanDbManager.getKanbanList();
//        mKanbanList = kanbanDbManager.getKanbanList();
    }

    private KanbanManager() {
        mKanbanList = new KanbanList();
        //get the kanbans from db
        //test
        Kanban kanban = new Kanban();
        kanban.setName("Welcome");
        mKanbanList.addKanban(kanban);
    }

    public static KanbanManager getInstance() {
        if (singleTon == null)
            singleTon = new KanbanManager();
        return singleTon;
    }

    public static KanbanManager getInstance(Context context) {
        if (singleTon == null)
            singleTon = new KanbanManager(context);
        return singleTon;
    }

    public KanbanList getmKanbanList() {
        return mKanbanList;
    }

    public void addKanban(String kanban) {
       kanbanDbManager.addKanban(kanban);
    }

    public void addTaskToKanban(String kanban, Task task) {
       kanbanDbManager.addTaskToKanban(kanban, task);
    }

    public void removeKanban(String kanban) {
        kanbanDbManager.deleteKanban(kanban);
    }

    public void removeTask(UUID taskUUID) {
        kanbanDbManager.removeTask(taskUUID);
    }

    public void changeTask(String kanban, Task task) {
        kanbanDbManager.changeTask(kanban, task);
    }

}
