package com.originals.myapplication.Model;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.originals.myapplication.Bean.Kanban;
import com.originals.myapplication.Bean.KanbanList;
import com.originals.myapplication.Bean.Task;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

public class KanbanDbManager {

    private KanbanDbHelper kanbanDbHelper;
    private SQLiteDatabase db;

    private static KanbanDbManager singleTon;

    private KanbanDbManager(Context context) {
        kanbanDbHelper = new KanbanDbHelper(context);
        db = kanbanDbHelper.getWritableDatabase();
    }

    public static KanbanDbManager getInstance(Context context) {
        if (singleTon == null) {
            singleTon = new KanbanDbManager(context);
        }
        return singleTon;
    }

    /**Kanban**/
    public KanbanList getKanbanList() {
        KanbanList kanbanList = new KanbanList();
        Cursor cursor = db.rawQuery("select * from kanbans", null);

        while (cursor.moveToNext()) {
            Kanban kanban = new Kanban();
            kanban.setName(cursor.getString(cursor.getColumnIndex("name")));
            int kanban_id = cursor.getInt(cursor.getColumnIndex("id"));
            Cursor taskCursor = db.rawQuery("select * from tasks where kanban_id=" + kanban_id, null);

            /**get task**/
            while (taskCursor.getCount() > 0 && taskCursor.moveToNext()) {
                Task task = new Task(taskCursor.getString(taskCursor.getColumnIndex("name")));
                task.setUuid(UUID.fromString(taskCursor.getString(taskCursor.getColumnIndex("uuid"))));

                String date = taskCursor.getString(taskCursor.getColumnIndex("date"));

                if (date.equals("null")) {
                    task.setDateLine(false);
                } else {
                    task.setDateLine(true);
                    task.setDateLine(date);
                }

                String description = taskCursor.getString(taskCursor.getColumnIndex("description"));

                if (description.equals("null")) {
                    task.setDescription(false);
                } else {
                    task.setDescription(true);
                    task.setDescription(description);
                }

                int id = taskCursor.getInt(taskCursor.getColumnIndex("id"));

                Cursor tagsCursor = db.rawQuery("select * from task_tags where task_id=\"" + id+"\"", null);

                /**get tag of task**/

                if (tagsCursor.isLast()) {
                    task.setTags(false);
                } else {
                    task.setTags(true);
                    while (tagsCursor.moveToNext()) {
                        task.addTag(tagsCursor.getString(tagsCursor.getColumnIndex("name")));
                    }
                }
                tagsCursor.close();
                kanban.addTask(task);
            }
            kanbanList.addKanban(kanban);
            taskCursor.close();
        }
        cursor.close();
        return kanbanList;
    }

    public void addKanban(String kanban) {
        db.execSQL("insert into kanbans(name) values(\"" + kanban + "\")");
    }

    public void deleteKanban(String kanban) {
        db.execSQL("delete from kanbans where name=\"" + kanban + "\"");
    }

    /**Task**/
    public void addTaskToKanban(String kanban, Task task) {
        //db.execSQL("insert into kanbans(name) values(" + kanban + ")");
        Cursor cursor = db.rawQuery("select id from kanbans where name=\""+kanban+"\"", null);

        if (cursor.moveToNext()) {
            int id = cursor.getInt(cursor.getColumnIndex("id"));

            String taskSql = "insert into tasks(kanban_id, uuid, name, date, description) values(%d,\"%s\",\"%s\",\"%s\",\"%s\")";

            String sql = String.format(taskSql, id,task.getUuid(), task.getTaskName(), task.getDateLine(), task.getDescription());
            db.execSQL(sql);

            Cursor taskCursor = db.rawQuery("select id from tasks where uuid=\""+ task.getUuid() + "\"", null);
            if (taskCursor.moveToNext()) {
                int task_id = taskCursor.getInt(taskCursor.getColumnIndex("id"));
                for (String tag : task.getTags()) {
                    Cursor tagCursor = db.rawQuery("select id from tags where name=\""+tag+"\"", null);
                    if (tagCursor.moveToNext()) {
                        int tag_id = tagCursor.getInt(tagCursor.getColumnIndex("id"));
                        db.execSQL("insert into task_tags(name,task_id,tag_id) values(\"" + tag+ "\"," + task_id + "," + tag_id + ")");
                    }
                    tagCursor.close();
                }
            }
            taskCursor.close();
        }
        cursor.close();

    }

    public void removeTask(UUID taskUUID) {
        db.execSQL("delete from tasks where uuid=\""+taskUUID.toString()+"\"");
    }

    public void changeTask(String kanban ,Task task) {
        removeTask(task.getUuid());
        addTaskToKanban(kanban, task);
    }

    /**Tag**/
    public List<String> getTags() {
        List<String> tagList = new ArrayList<>();
        Cursor cursor = db.rawQuery("select name from tags", null);
        while (cursor.moveToNext()) {
           tagList.add(cursor.getString(cursor.getColumnIndex("name")));
        }
        cursor.close();
        return tagList;
    }

    public void removeTag(String tag) {
        db.execSQL("delete from tags where name=\"" + tag + "\"");
    }

    public void addNewTag(String tag) {
        db.execSQL("insert into tags(name) values(\"" + tag + "\")");
    }

    public boolean addTagForTask(String tag, UUID taskID) {
        int task_id, tag_id;

        Cursor cursor = db.rawQuery("select id from tasks where uuid=" + taskID.toString(), null);

        if (cursor.moveToNext()) {
            task_id = cursor.getInt(cursor.getColumnIndex("id"));
        } else {
            return false;
        }

        cursor.close();
        cursor = db.rawQuery("select id from tags where name=" + tag, null);

        if (cursor.moveToNext()) {
           tag_id = cursor.getInt(cursor.getColumnIndex("id"));
        } else {
            return false;
        }

        db.execSQL("insert into task_tags(name, task_id, tag_id) values(" + tag + "," + task_id + "," + tag_id +")");
        return false;
    }
}
