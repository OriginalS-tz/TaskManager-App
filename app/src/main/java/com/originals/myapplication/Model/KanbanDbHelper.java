package com.originals.myapplication.Model;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class KanbanDbHelper extends SQLiteOpenHelper{
    private static final int VERSION = 1;
    private static final String DB_NAME = "kanban.db";

    public KanbanDbHelper(Context context) {
        super(context, DB_NAME, null, VERSION);
    }

    @Override
    public void onOpen(SQLiteDatabase db) {
        super.onOpen(db);
        if (!db.isReadOnly()) {
            db.execSQL("PRAGMA foreign_keys=ON;");
        }
    }

    @Override
    public void onCreate(SQLiteDatabase db) {

        db.execSQL("create table kanbans " +
                "(id integer primary key autoincrement," +
                " name string not null)");

        db.execSQL("insert into kanbans(name) values(\"welcome\")");

        db.execSQL("create table tasks " +
                "(id integer primary key autoincrement," +
                "kanban_id integer not null," +
                "uuid string not null," +
                "name string not null, " +
                "date string, " +
                "description string," +
                "foreign key (kanban_id) references kanbans(id) on delete cascade on update cascade)");

        db.execSQL("create table task_tags " +
                "(id integer primary key autoincrement, " +
                "name string not null," +
                "task_id integer," +
                "tag_id integer not null,"+
                "foreign key (task_id) references tasks(id) on delete cascade on update cascade," +
                "foreign key (tag_id) references tags(id) on delete cascade on update cascade)");

        db.execSQL("create table tags " +
                "(id integer primary key autoincrement," +
                " name string not null)");
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

    }
}
