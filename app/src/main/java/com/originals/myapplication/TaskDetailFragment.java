package com.originals.myapplication;

import android.support.v4.app.Fragment;

public class TaskDetailFragment extends SingleFragmentActivity {
    @Override
    protected Fragment createFragment() {
        return new TaskDetailActivity();
    }
}
