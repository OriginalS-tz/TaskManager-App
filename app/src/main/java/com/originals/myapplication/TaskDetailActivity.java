package com.originals.myapplication;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v7.widget.Toolbar;
import android.view.ContextMenu;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.SubMenu;
import android.view.View;
import android.view.ViewGroup;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.originals.myapplication.Controller.TagsController;
import com.originals.myapplication.Bean.Task;
import com.originals.myapplication.Bean.VALUES;
import com.yang.flowlayoutlibrary.FlowLayout;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;

public class TaskDetailActivity extends Fragment {

    private View v;
    private Intent intent;
    private Toolbar toolbar;
    private List<String> mTagList;
    private TagsController tagsController;
    private Task changeTask;
    private int resultCode;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        intent = getActivity().getIntent();
        //tagsController = new TagsController();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        v = inflater.inflate(R.layout.task_detail, container, false);

       tagsController = new TagsController(v.getContext().getApplicationContext());

        setTagsAddButton();
        initToolbar();
        Task task = (Task) intent.getSerializableExtra("task");

        resultCode = VALUES.NEW_TASK_REQUEST_CODE;
        if (task != null) {
            changeTask = task;
            toolbar.setTitle("TASK");
            resultCode = VALUES.TASK_INFO_CHANGE_CODE;
            EditText editText = v.findViewById(R.id.newTaskName);
            editText.setText(task.getTaskName());

            if (task.hasTags()) {
                mTagList = task.getTags();
                setTags();
            }

            if (task.hasDateLine()) {
                setDateLine(task.getDateLine());
            }

            if (task.hasDescription()) {
                TextView textView = v.findViewById(R.id.description);
                textView.setText(task.getDescription());
                textView.setVisibility(View.VISIBLE);
            }
        }
        //setTags();
        return v;
    }

    @Override
    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
        super.onCreateContextMenu(menu, v, menuInfo);
        //itme_list:
        // item: add description
        // item: add dateline
        menu.add(0, VALUES.ADD_DESCRIPTION, 0, "Add Description");
        menu.add(0, VALUES.ADD_DATELINE, 0, "Add Dateline");
        SubMenu tags =  menu.addSubMenu(0, 0, 0, "Tags");
        if (resultCode == VALUES.TASK_INFO_CHANGE_CODE) {
            menu.add(0, VALUES.DELETE_TASK, 0, "Delete Task");
        }

        SubMenu tagsList = tags.addSubMenu(0, VALUES.ADD_TAGS, 0, "Choose Tag");
        SubMenu tagsManager = tags.addSubMenu(0, 0, 0, "Delete Tag");

        tagsList.add(0, VALUES.NEW_TAG, 0,"New A Tag.....");
        for (String tag : tagsController.getTags()) {
            tagsManager.add(0, VALUES.TASK_DELETE, 0, tag);
            tagsList.add(0, VALUES.TAGS,0, tag);
        }
    }

    @Override
    public boolean onContextItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case VALUES.TAGS:{
                String tagName = item.getTitle().toString();
                for (String tag : tagsController.getTags()) {
                    if (tagName.equals(tag)) {
                        addTag(tag);
                        break;
                    }
                }
                break;
            }
            case VALUES.ADD_DESCRIPTION: {
                v.findViewById(R.id.description).setVisibility(View.VISIBLE);
                break;
            }
            case VALUES.DELETE_TASK: {
                removeTask();
                break;
            }
            case VALUES.ADD_DATELINE: {
                View view = LayoutInflater.from(getActivity()).inflate(R.layout.date_dialog, null);
                final DatePicker datePicker = view.findViewById(R.id.date_dialog);
                new AlertDialog.Builder(getActivity())
                         .setView(view)
                         .setPositiveButton("ok", new DialogInterface.OnClickListener() {
                             @Override
                             public void onClick(DialogInterface dialog, int which) {
                                 Date date = new GregorianCalendar(
                                        datePicker.getYear(),
                                        datePicker.getMonth(),
                                        datePicker.getDayOfMonth()
                                 ).getTime();
                                 SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");
                                 String mDate = simpleDateFormat.format(date);
                                 setDateLine(mDate);
                             }
                         }).create().show();
                break;
            }
            case VALUES.NEW_TAG: {
                newTagDialog();
                break;
            }
            case VALUES.TASK_DELETE: {
                tagsController.removeTag(item.getTitle().toString());
                mTagList.remove(item.getTitle().toString());
                setTags();
                break;
            }

        }
        return super.onContextItemSelected(item);
    }

    public void newTagDialog() {
        final EditText editText = new EditText(v.getContext());
        new AlertDialog.Builder(getActivity())
                .setView(editText)
                .setPositiveButton("add", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        String newTag = editText.getText().toString();
                        if (tagsController.addTag(newTag)) {
                            addTag(newTag);
                            Toast.makeText(v.getContext(), "add a new tag", Toast.LENGTH_LONG).show();
                        } else {
                            Toast.makeText(v.getContext(), "the tag is exist", Toast.LENGTH_LONG).show();
                        }
                    }
                }).show();
    }

    private void initToolbar() {
        toolbar = v.findViewById(R.id.task_detail_toolbar);
        toolbar.inflateMenu(R.menu.new_task_menu);
        final EditText editText = v.findViewById(R.id.newTaskName);
        toolbar.setOnMenuItemClickListener(new Toolbar.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem item) {
                if (item.getItemId() == R.id.ok) {
                    String taskName = editText.getText().toString();
                    if (taskName.length() != 0) {
                        //Toast.makeText(v.getContext(),"add",Toast.LENGTH_LONG).show();
                        sendTask(taskName, resultCode                         );
                    }
                }
                return true;
            }
        });
    }

    private void setTagsAddButton() {
        final FloatingActionButton FAB = v.findViewById(R.id.edit_button);
        mTagList = new ArrayList<>();
        registerForContextMenu(FAB);
        FAB.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                FAB.showContextMenu();
            }
        });
    }

    private void addTag(String tag) {
        if (mTagList.size() < 3) {
            for (String mTag : mTagList) {
                if (mTag.equals(tag)) {
                    return;
                }
            }
            mTagList.add(tag);
            setTags();
            Toast.makeText(v.getContext(), "list size:" + mTagList.size(), Toast.LENGTH_SHORT).show();
        }
    }

    private void setTags() {
        FlowLayout flKeyword = v.findViewById(R.id.fl_keyword);
        flKeyword.removeAllViews();
        // 设置文字大小
        flKeyword.setTextSize(15);
        // 设置文字颜色
        flKeyword.setTextColor(Color.BLACK);
        // 设置文字背景
        flKeyword.setBackgroundResource(R.drawable.bg_frame);

        // 设置文字水平margin
        flKeyword.setHorizontalSpacing(15);
        // 设置文字垂直margin
        flKeyword.setVerticalSpacing(15);

        // 设置文字水平padding
        flKeyword.setTextPaddingH(15);
        // 设置文字垂直padding
        flKeyword.setTextPaddingH(8);

        // 设置UI与点击事件监听
        // 最后调用setFlowLayout方法
        flKeyword.setFlowLayout(mTagList, new FlowLayout.OnItemClickListener() {
            @Override
            public void onItemClick(String content) {
                mTagList.remove(content);
                Toast.makeText(v.getContext(), "mTagList size : :" + mTagList.size(), Toast.LENGTH_SHORT).show();

                setTags();
            }
        });
    }

    private void setDateLine(String dateLine) {
        TextView textView = v.findViewById(R.id.dateline);
        textView.setText(dateLine);
    }

    private void sendTask(String taskName, int resultCode) {
        Task task;
        if (resultCode == VALUES.TASK_INFO_CHANGE_CODE)
            task = changeTask;
        else
            task = new Task(taskName);

        if (mTagList.size() > 0) {
            task.setTags(true);
            task.setTags(mTagList);
        } else {
            task.setTags(false);
        }

        TextView descriptionArea = v.findViewById(R.id.description);
        String description = descriptionArea.getText().toString();

        if (description.length() > 0) {
            task.setDescription(true);
            task.setDescription(description);
        }

        TextView dateLineView = v.findViewById(R.id.dateline);
        String dateLine = dateLineView.getText().toString();
        if (dateLine.length() > 0) {
            task.setDateLine(true);
            task.setDateLine(dateLine);
        }

        Intent intent = new Intent();
        intent.putExtra("task", task);

        getActivity().setResult(resultCode,intent);
        getActivity().onBackPressed();
    }

    private void removeTask() {
        getActivity().setResult(VALUES.DELETE_TASK);
        getActivity().onBackPressed();
    }

    @Override
    protected void finalize() throws Throwable {
        super.finalize();
    }
}
