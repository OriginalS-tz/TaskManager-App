# TaskManager

## Introduction

this app is my homework, it is a task management application

support tag, dateline, description of the task

## Structure

![relationship](./img/relationship.png)

Base on MVC pattern

### KanbanFragment

![kanbanfragment](./img/kanbanfragment.png)

### TaskDetailFragment

![taskdetailfragment](./img/taskdetailfragment.png)

### Managers

Managers use KanbanDbManager to communicate with the Sqlite DataBase

### DataBase

![database](./img/database.png)

```java
db.execSQL("create table kanbans " +
                "(id integer primary key autoincrement," +
                " name string not null)");

        db.execSQL("insert into kanbans(name) values(\"welcome\")");

        db.execSQL("create table tasks " +
                "(id integer primary key autoincrement," +
                "kanban_id integer not null," +
                "uuid string not null," +
                "name string not null, " +
                "date string, " +
                "description string," +
                "foreign key (kanban_id) references kanbans(id) on delete cascade on update cascade)");

        db.execSQL("create table task_tags " +
                "(id integer primary key autoincrement, " +
                "name string not null," +
                "task_id integer," +
                "tag_id integer not null,"+
                "foreign key (task_id) references tasks(id) on delete cascade on update cascade," +
                "foreign key (tag_id) references tags(id) on delete cascade on update cascade)");

        db.execSQL("create table tags " +
                "(id integer primary key autoincrement," +
                " name string not null)");
```

### Layout

![layout](./img/layout.png)

## About

the tag support use [alidili/FlowLayout](https://github.com/alidili/FlowLayout)

this  is a debug version, you can download the zip and use Android Studio to open it and run it on the VM or your android phone





